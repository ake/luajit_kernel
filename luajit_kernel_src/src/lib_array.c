/*
** Array library.
** Copyright (C) 2005-2014 Mike Pall. See Copyright Notice in luajit.h
*/

#define lib_array_c
#define LUA_LIB

#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"

#include "lj_obj.h"
#include "lj_err.h"
#include "lj_str.h"
#include "lj_lib.h"
#include "lj_tab.h"

#include <sys/param.h>
#include <sys/systm.h>
#include <sys/proc.h>
#include <sys/module.h>
#include <sys/sysproto.h>
#include <sys/sysent.h>
#include <sys/kernel.h>
#include <sys/systm.h>

/* ------------------------------------------------------------------------ */

#define LJLIB_MODULE_array

struct num_array {
  size_t len;
  int32_t values[1];
};

LJLIB_CF(array_new) {
  struct num_array *array = NULL;
  
  int n = lj_lib_checkint(L, 1);
  if (n < 1) {
	return luaL_argerror(L, 1, "Size must be at least 1");
  }

  size_t nbytes = sizeof(struct num_array) + ((n - 1) * sizeof(int32_t));
  array = lua_newuserdata(L, nbytes);	

  if (array == NULL) {
	luaL_error(L, "Cannot not allocate an array: out of memory?");
  }

  array->len = n;
  bzero(array->values, n * sizeof(int32_t));
  
  return 1;
}

LJLIB_CF(array_set) {
  struct num_array *array = lua_touserdata(L, 1);
  int index = lj_lib_checkint(L, 2) - 1;
  int value = lj_lib_checkint(L, 3);

  if (array == NULL) {
	return luaL_argerror(L, 1, "Expect an array");
  }

  if (index < 0 || index > array->len) {
	return luaL_argerror(L, 2, "Index out of bound");
  }

  array->values[index] = value;

  return 0;
}

LJLIB_CF(array_get) {
  struct num_array *array = lua_touserdata(L, 1);
  int index = lj_lib_checkint(L, 2) - 1;

  if (array == NULL) {
	return luaL_argerror(L, 1, "Expect an array");
  }

  if (index < 0 || index > array->len) {
	return luaL_argerror(L, 2, "Index out of bound");
  }

  lua_pushinteger(L, array->values[index]);

  return 1;
}

LJLIB_CF(array_getsize) {
  struct num_array *array = lua_touserdata(L, 1);

  if (array == NULL) {
	return luaL_argerror(L, 1, "Expect an array");
  }

  lua_pushinteger(L, array->len);

  return 1;
}


/* ------------------------------------------------------------------------ */

#include "lj_libdef.h"

LUALIB_API int luaopen_array(lua_State *L)
{
  LJ_LIB_REG(L, "array", array);
  return 1;
}

