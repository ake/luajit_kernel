/*
** System call library.
** Copyright (C) 2005-2014 Mike Pall. See Copyright Notice in luajit.h
*/

#define lib_syscall_c
#define LUA_LIB

#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"

#include "lj_obj.h"
#include "lj_err.h"
#include "lj_str.h"
#include "lj_lib.h"
#include "lj_tab.h"

#include <sys/param.h>
#include <sys/systm.h>
#include <sys/proc.h>
#include <sys/module.h>
#include <sys/sysproto.h>
#include <sys/sysent.h>
#include <sys/kernel.h>
#include <sys/systm.h>

/* ------------------------------------------------------------------------ */

#define LJLIB_MODULE_syscall

int lj_vm_syscall_read(int32_t fd, int32_t buf, int32_t nbytes)
{
  struct read_args args = {
	.fd = fd,
	.buf = (void *)buf,
	.nbyte = (size_t)nbytes
  };

  int error = sys_read(curthread, &args);
  if (error) {
	return -error;
  }

  return curthread->td_retval[0];
}

int lj_vm_syscall_write(int32_t fd, int32_t buf, int32_t nbytes)
{
  struct write_args args = {
	.fd = fd,
	.buf = (void *)buf,
	.nbyte = (size_t)nbytes
  };

  int error = sys_write(curthread, &args);
  if (error) {
	return -error;
  }

  return curthread->td_retval[0];
}

int lj_vm_syscall_sendmsg(int32_t s, int32_t message, int32_t flags)
{
  struct sendmsg_args args = {
	.s = s,
	.msg = (struct msghdr *)message,
	.flags = flags
  };

  int error = sys_sendmsg(curthread, &args);
  if (error) {
	return -error;
  }

  return curthread->td_retval[0];
}

int lj_vm_syscall_sendto(int32_t s, int32_t buf, int32_t len, int32_t flags, int32_t to, int32_t tolen)
{
  struct sendto_args args = {
	.s = s,
	.buf = (caddr_t)buf,
	.len = (size_t)len,
	.flags = flags,
	.to = (caddr_t)to,
	.tolen = tolen
  };

  int error = sys_sendto(curthread, &args);
  if (error) {
	return -error;
  }

  return curthread->td_retval[0];
}

int lj_vm_syscall_recvfrom(int32_t s, int32_t buf, int32_t length, int32_t flags, int32_t addr, int32_t addr_len)
{
  struct recvfrom_args args = {
	.s = s,
	.buf = (caddr_t)buf,
	.len = (size_t)length,
	.flags = flags,
	.from = (struct sockaddr *)addr,
	.fromlenaddr = (__socklen_t *)addr_len
  };

  int error = sys_recvfrom(curthread, &args);
  if (error) {
	return -error;
  }

  return curthread->td_retval[0];
}

int lj_vm_syscall_accept(int32_t s, int32_t addr, int32_t addrlen)
{
  struct accept_args args = {
	.s = s,
	.name = (struct sockaddr *)addr,
	.anamelen = (__socklen_t *)addrlen
  };

  int error = sys_accept(curthread, &args);
  if (error) {
	return -error;
  }

  return curthread->td_retval[0];
}

int lj_vm_syscall_accept4(int32_t s, int32_t addr, int32_t addrlen, int32_t flags)
{
  struct accept4_args args = {
	.s = s,
	.name = (struct sockaddr *)addr,
	.anamelen = (__socklen_t *)addrlen,
	.flags = flags
  };

  int error = sys_accept4(curthread, &args);
  if (error) {
	return -error;
  }

  return curthread->td_retval[0];
}

int lj_vm_syscall_kevent(int32_t kq, int32_t changelist, int32_t nchanges, int32_t eventlist, int32_t nevents, int32_t timeout)
{
  struct kevent_args args = {
	.fd = kq,
	.changelist = (struct kevent *)changelist,
	.nchanges = nchanges,
	.eventlist = (struct kevent *)eventlist,
	.nevents = nevents,
	.timeout = (struct timespec *)timeout
  };

  int error = sys_kevent(curthread, &args);
  if (error) {
	return -error;
  }

  return curthread->td_retval[0];
}

int lj_vm_syscall_ioctl(int32_t fd, int32_t request, int32_t data)
{
  struct ioctl_args args = {
	.fd = fd,
	.com = (u_long)request,
	.data = (caddr_t)data
  };

  int error = sys_ioctl(curthread, &args);
  if (error) {
	return -error;
  }

  return 0;
}

LJLIB_CF(syscall_read)	LJLIB_REC(.)
{
  int32_t fd = lj_lib_checkint(L, 1);
  int32_t buf = lj_lib_checkint(L, 2);
  int32_t nbytes = lj_lib_checkint(L, 3);

  int result = lj_vm_syscall_read(fd, buf, nbytes);
  lua_pushinteger(L, result);

  return 1;
}

LJLIB_CF(syscall_write)	LJLIB_REC(.)
{
  int32_t fd = lj_lib_checkint(L, 1);
  int32_t buf = lj_lib_checkint(L, 2);
  int32_t nbytes = lj_lib_checkint(L, 3);

  int result = lj_vm_syscall_write(fd, buf, nbytes);
  lua_pushinteger(L, result);

  return 1;
}

LJLIB_CF(syscall_sendmsg)	LJLIB_REC(.)
{
  int32_t s = lj_lib_checkint(L, 1);
  int32_t msg = lj_lib_checkint(L, 2);
  int32_t flags = lj_lib_checkint(L, 3);

  int result = lj_vm_syscall_sendmsg(s, msg, flags);
  lua_pushinteger(L, result);

  return 1;
}

LJLIB_CF(syscall_sendto)	LJLIB_REC(.)
{
  int32_t s = lj_lib_checkint(L, 1);
  int32_t buf = lj_lib_checkint(L, 2);
  int32_t len = lj_lib_checkint(L, 3);
  int32_t flags = lj_lib_checkint(L, 4);
  int32_t to = lj_lib_checkint(L, 5);
  int32_t tolen = lj_lib_checkint(L, 6);

  int result = lj_vm_syscall_sendto(s, buf, len, flags, to, tolen);
  lua_pushinteger(L, result);

  return 1;
}

LJLIB_CF(syscall_recvfrom)	LJLIB_REC(.)
{
  int32_t s = lj_lib_checkint(L, 1);
  int32_t buf = lj_lib_checkint(L, 2);
  int32_t length = lj_lib_checkint(L, 3);
  int32_t flags = lj_lib_checkint(L, 4);
  int32_t addr = lj_lib_checkint(L, 5);
  int32_t addr_len = lj_lib_checkint(L, 6);

  int result = lj_vm_syscall_recvfrom(s, buf, length, flags, addr, addr_len);
  lua_pushinteger(L, result);

  return 1;
}

LJLIB_CF(syscall_accept)	LJLIB_REC(.)
{
  int32_t s = lj_lib_checkint(L, 1);
  int32_t addr = lj_lib_checkint(L, 2);
  int32_t addrlen = lj_lib_checkint(L, 3);

  int result = lj_vm_syscall_accept(s, addr, addrlen);
  lua_pushinteger(L, result);

  return 1;
}

LJLIB_CF(syscall_accept4)	LJLIB_REC(.)
{
  int32_t s = lj_lib_checkint(L, 1);
  int32_t addr = lj_lib_checkint(L, 2);
  int32_t addrlen = lj_lib_checkint(L, 3);
  int32_t flags = lj_lib_checkint(L, 4);

  int result = lj_vm_syscall_accept4(s, addr, addrlen, flags);
  lua_pushinteger(L, result);

  return 1;
}

LJLIB_CF(syscall_kevent)	LJLIB_REC(.)
{
  int32_t kq = lj_lib_checkint(L, 1);
  int32_t changelist = lj_lib_checkint(L, 2);
  int32_t nchanges = lj_lib_checkint(L, 3);
  int32_t eventlist = lj_lib_checkint(L, 4);
  int32_t nevents = lj_lib_checkint(L, 5);
  int32_t timeout = lj_lib_checkint(L, 6);

  int result = lj_vm_syscall_kevent(kq, changelist, nchanges, eventlist, nevents, timeout);
  lua_pushinteger(L, result);

  return 1;
}

LJLIB_CF(syscall_ioctl)	LJLIB_REC(.)
{
  int32_t fd = lj_lib_checkint(L, 1);
  int32_t request = lj_lib_checkint(L, 2);
  int32_t data = lj_lib_checkint(L, 3);

  int result = lj_vm_syscall_ioctl(fd, request, data);
  lua_pushinteger(L, result);

  return 1;
}

/* ------------------------------------------------------------------------ */

#include "lj_libdef.h"

LUALIB_API int luaopen_syscall(lua_State *L)
{
  LJ_LIB_REG(L, "syscall", syscall);
  return 1;
}

