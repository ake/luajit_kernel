/*
** System call library.
** Copyright (C) 2005-2014 Mike Pall. See Copyright Notice in luajit.h
*/

#define lib_util_c
#define LUA_LIB

#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"

#include "lj_obj.h"
#include "lj_err.h"
#include "lj_str.h"
#include "lj_lib.h"
#include "lj_tab.h"

#include <sys/param.h>
#include <sys/systm.h>
#include <sys/proc.h>
#include <sys/module.h>
#include <sys/sysproto.h>
#include <sys/sysent.h>
#include <sys/kernel.h>
#include <sys/systm.h>

/* ------------------------------------------------------------------------ */

#define LJLIB_MODULE_util

struct num_array {
  size_t len;
  int32_t values[1];
};

int lj_vm_util_copyout(GCtab *t, int32_t u_addr, int32_t nslot) {
  int ret = 0;
  /* XXX: should limit the objlen? */
  size_t len = lj_tab_len(t);
  if (len <= 0 || nslot <= 0 || len < nslot) {
    ret = -1;
	goto end;
  }

  unsigned int i;
  int32_t *out = malloc(len * sizeof(int32_t), M_TEMP, M_NOWAIT);
  if (out == NULL) {
    ret = -1;
	goto end;
  }

  for (i = 0; i < len; i++) {
    cTValue *v = lj_tab_getint(t, i + 1);
	int32_t val = tvisint(v) ? intV(v) : 0;
	out[i] = val;
  }
  
  int error = copyout(out, (void *)u_addr, len * sizeof(int32_t));
  if (error) {
	ret = -error;
	/* Fall through */
  }

  free(out, M_TEMP);
end:
  return ret;
}

int LJ_FASTCALL lj_vm_util_fuword(int32_t u_addr) {
  return fuword((void *)u_addr);
}

LJLIB_CF(util_copyout)	LJLIB_REC(.)
{
  GCtab *t = lj_lib_checktab(L, 1); 
  int32_t u_addr = lj_lib_checkint(L, 2);
  int32_t nslot = lj_lib_checkint(L, 3);

  int result = lj_vm_util_copyout(t, u_addr, nslot);
  lua_pushinteger(L, result);

  return 1;
}

/* XXX: Still cannot find a proper way to JIT compler */
LJLIB_CF(util_copyin)
{
  int32_t u_addr = lj_lib_checkint(L, 1);
  int32_t nslot = lj_lib_checkint(L, 2);

  if (nslot <= 0) {
    lua_pushinteger(L, -1);
    goto end;
  }

  int32_t *in = malloc(nslot * sizeof(int32_t), M_TEMP, M_NOWAIT);
  if (in == NULL) {
    lua_pushinteger(L, -1);
    goto end;
  }

  int error = copyin((void *)u_addr, in, nslot * sizeof(int32_t));
  if (error) {
    lua_pushinteger(L, -1);
    goto free_before_end;
  }

  GCtab *t;

  /* Create a table, the table is on the top of stack */
  lua_createtable(L, nslot, 0);

  t = tabV(L->top-1);

  unsigned int i;
  for (i = 0; i < nslot; i++) {
    setintV(lj_tab_setint(L, t, i + 1), in[i]);
  }

free_before_end:
  free(in, M_TEMP);
end:
  return 1;
}

LJLIB_CF(util_fuword)	LJLIB_REC(.)
{
  int32_t u_addr = lj_lib_checkint(L, 1);

  int result = lj_vm_util_fuword(u_addr);
  lua_pushinteger(L, result);

  return 1;
}

/* ------------------------------------------------------------------------ */

#include "lj_libdef.h"

LUALIB_API int luaopen_util(lua_State *L)
{
  LJ_LIB_REG(L, "util", util);
  return 1;
}

