# Makefile for building the sample syscall module
# $FreeBSD: release/10.0.0/share/examples/kld/syscall/module/Makefile 83608 2001-09-18 12:03:42Z ru $

KMOD=	luajit_kernel
SRCS=	luajit_kernel.c

# Suppress the warning
MAKEOBJDIR:= $(.CURDIR)

LUAJIT:=			$(.CURDIR)/luajit_kernel_src
LUAJIT_SRC:= 		$(LUAJIT)/src
LUAJIT_SRC_HOST:= 	$(LUAJIT_SRC)/host
DYNASM:=			$(LUAJIT)/dynasm

MINILUA:= 	$(LUAJIT_SRC_HOST)/minilua
MINILUA_O:= $(MINILUA).o

BUILDVM:=	$(LUAJIT_SRC_HOST)/buildvm

BUILDVM_O:= buildvm.o buildvm_asm.o buildvm_fold.o buildvm_lib.o buildvm_peobj.o
BUILDVM_O:= ${BUILDVM_O:%.o=$(LUAJIT_SRC_HOST)/%.o}

BUILDVM_GEN_H:= $(LUAJIT_SRC_HOST)/buildvm_arch.h

BUILDVM_CFLAGS:= 	-I$(LUAJIT_SRC) \
					-O2 -fomit-frame-pointer -Wall \
					-DLUAJIT_DISABLE_FFI \
					-DLUAJIT_NUMMODE=2 \
					-DLUAJIT_TARGET=LUAJIT_ARCH_x86 \
					-DLUAJIT_NO_UNWIND=1 \
					-DLJ_ARCH_HASFPU=1 \
					-DLJ_ABI_SOFTFP=0 \
					-D_BUILDVM_KERNEL=1

LUAJIT_GEN_H:= lj_bcdef.h lj_ffdef.h lj_libdef.h lj_recdef.h lj_folddef.h
LUAJIT_GEN_H:= ${LUAJIT_GEN_H:%.h=$(LUAJIT_SRC)/%.h}

LUAJIT_GEN_S:= lj_vm.s
LUAJIT_GEN_O:= $(LUAJIT_GEN_S:.s=.o)

LIB_VMDEF:= $(LUAJIT_SRC)/jit/vmdef.lua
LIB_VMDEFP:= $(LIB_VMDEF)

LUAJIT_GEN:= $(LUAJIT_GEN_S) $(LUAJIT_GEN_O) $(LUAJIT_GEN_H) $(LIB_VMDEFP) 

.PATH: $(LUAJIT_SRC) $(.PATH)

LUAJIT_LIB_O:= lib_base.o lib_bit.o lib_string.o lib_table.o \
				lib_package.o lib_debug.o lib_jit.o lib_syscall.o \
				lib_util.o lib_array.o
LUAJIT_LIB_C:= $(LUAJIT_LIB_O:%.o=$(LUAJIT_SRC)/%.c)

# TODO: Add header dependencies (not a serious issue)
LUAJIT_CORE_O:= lj_gc.o lj_err.o lj_char.o lj_bc.o lj_obj.o \
	  lj_str.o lj_tab.o lj_func.o lj_udata.o lj_meta.o lj_debug.o \
	  lj_state.o lj_dispatch.o lj_vmevent.o lj_vmmath.o lj_strscan.o \
	  lj_api.o lj_lex.o lj_parse.o lj_bcread.o lj_bcwrite.o lj_load.o \
	  lj_ir.o lj_opt_mem.o lj_opt_fold.o lj_opt_narrow.o \
	  lj_opt_dce.o lj_opt_loop.o lj_opt_split.o lj_opt_sink.o \
	  lj_mcode.o lj_snap.o lj_record.o lj_crecord.o lj_ffrecord.o \
	  lj_asm.o lj_trace.o lj_gdbjit.o \
	  lj_ctype.o lj_cdata.o lj_cconv.o lj_ccall.o lj_ccallback.o \
	  lj_carith.o lj_clib.o lj_cparse.o \
	  lj_lib.o lj_alloc.o lib_aux.o \
	  lib_init.o $(LUAJIT_LIB_O)

SRCS+= ${LUAJIT_CORE_O:.o=.c}
OBJS+= $(LUAJIT_GEN_O)

CFLAGS+=	-I$(MAKEOBJDIR)/@/sys	\
			-I$(MAKEOBJDIR)/@/x86/include \
			-I$(LUAJIT_SRC) \
			-DLUAJIT_DISABLE_FFI \
			-DLUAJIT_NUMMODE=2 \
			-DLUAJIT_TARGET=LUAJIT_ARCH_x86 \
			-DLUAJIT_NO_UNWIND=1 \
			-DLJ_ARCH_HASFPU=1 \
			-DLJ_ABI_SOFTFP=0

luajit_kernel.c: $(LUAJIT_GEN) $(LUAJIT_CORE_O)

$(MINILUA): $(MINILUA_O)
	$(CC) -o $(.TARGET) $(.ALLSRC) -lm

$(MINILUA_O): $(MINILUA).c
	$(CC) -O2 -fomit-frame-pointer -Wall -c -o $(.TARGET) $(.IMPSRC)

$(BUILDVM): $(BUILDVM_O)
	$(CC) -o $(.TARGET) $(.ALLSRC)

$(BUILDVM_O): $(BUILDVM_GEN_H) $(.PREFIX).c 
	$(CC) $(BUILDVM_CFLAGS) -c -o $(.TARGET) $(.IMPSRC)

$(LUAJIT_SRC_HOST)/buildvm.o: $(DYNASM)/dasm_*.h 

$(BUILDVM_GEN_H): $(MINILUA)
	$(MINILUA) $(DYNASM)/dynasm.lua -D _KERNEL -D JIT -D DUALNUM -D FPU -D HFABI -D VER= -o $(LUAJIT_SRC_HOST)/buildvm_arch.h $(LUAJIT_SRC)/vm_x86.dasc

$(LUAJIT_GEN_O): $(LUAJIT_GEN_S)
	$(CC) -c -o $(.TARGET) $(.PREFIX).s

$(LUAJIT_GEN_S): $(BUILDVM)
	$(BUILDVM) -m elfasm -o $(.TARGET)

$(LUAJIT_SRC)/lj_bcdef.h: $(BUILDVM) $(LUAJIT_LIB_C)
	$(BUILDVM) -m bcdef -o $(.TARGET) $(LUAJIT_LIB_C)

$(LUAJIT_SRC)/lj_ffdef.h: $(BUILDVM) $(LUAJIT_LIB_C)
	$(BUILDVM) -m ffdef -o $(.TARGET) $(LUAJIT_LIB_C)

$(LUAJIT_SRC)/lj_libdef.h: $(BUILDVM) $(LUAJIT_LIB_C)
	$(BUILDVM) -m libdef -o $(.TARGET) $(LUAJIT_LIB_C)

$(LUAJIT_SRC)/lj_recdef.h: $(BUILDVM) $(LUAJIT_LIB_C)
	$(BUILDVM) -m recdef -o $(.TARGET) $(LUAJIT_LIB_C)

$(LIB_VMDEF): $(BUILDVM) $(LUAJIT_LIB_C)
	$(BUILDVM) -m vmdef -o $(LIB_VMDEFP) $(LUAJIT_LIB_C)

$(LUAJIT_SRC)/lj_folddef.h: $(BUILDVM) $(LUAJIT_SRC)/lj_opt_fold.c
	$(BUILDVM) -m folddef -o $(.TARGET) $(LUAJIT_SRC)/lj_opt_fold.c

.include <bsd.kmod.mk>

.PHONY+= luajit_clean

CFLAGS:= ${CFLAGS:-Wundef=}
CFLAGS:= ${CFLAGS:-Wmissing-prototypes=}

clean: luajit_clean

luajit_clean:
	rm -f $(MINILUA) $(MINILUA_O)
	rm -f $(BUILDVM_GEN_H) $(BUILDVM_O) $(BUILDVM)
	rm -f $(LUAJIT_GEN)

